using System.Collections.Generic;

namespace UnityEngine.Replay
{
    /// <summary>
    ///     The top level browser for managing carousels and listings
    /// </summary>
    public class UIBrowser : MonoBehaviour
    {
        public static UIBrowser instance { get; private set; }

        /// <summary>
        ///  Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            instance = this;
        }

        /// <summary>
        /// Initialize the browser
        /// </summary>
        /// <param name="listings">The listings to populate the carosel with</param>
        public void Init(List<Listing> listings)
        {
            UICarousel carousel = FindObjectOfType<UICarousel>();
            if (carousel != null) {
                carousel.Init(listings[0].category, listings);
            } else {
                Debug.LogError("UICarousel not found");
            }
        }

    }
}
