using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Replay;

namespace Unity.Metacast.Demo
{
    /// <summary>
    ///     Populate UIBrowser with test json data
    /// </summary>
    public class TestListings : MonoBehaviour
    {
        private List<Listing> listings = new List<Listing>();

        /// <summary>
        ///     Start is called on the frame when a script is enabled just
        ///     before any of the Update methods are called the first time.
        /// </summary>
        private IEnumerator Start()
        {
			yield return StartCoroutine(FetchGames());
			yield return StartCoroutine(LoadImages());
            UIBrowser.instance.Init(listings);
        }

        /// <summary>
        ///     Fetches all of the game listings and initializes the UI
        /// </summary>
		#nullable enable
        IEnumerator FetchGames()
        {
            string? nextPage = "http://localhost:8000/api/games/feed";
            while (nextPage != "") {
                using (UnityWebRequest webRequest = UnityWebRequest.Get(nextPage))
                {
                    yield return webRequest.SendWebRequest();

                    switch (webRequest.result)
                    {
                        case UnityWebRequest.Result.ConnectionError:
                        case UnityWebRequest.Result.DataProcessingError:
                        case UnityWebRequest.Result.ProtocolError:
                            Debug.LogError(": Error: " + webRequest.error);
                            throw new ApplicationException();
                        case UnityWebRequest.Result.Success:
                            break;
                    }
                    MetacastAPIPage<Listing> page = JsonUtility.FromJson<MetacastAPIPage<Listing>>(
                        webRequest.downloadHandler.text);


                    nextPage = page.next;
                    listings.AddRange(page.results);
                }
            }
        }

        /// <summary>
        ///     Fetches all images for each game
        /// </summary>
        IEnumerator LoadImages() {
            for(int i = 0; i < listings.Count; i++) {
                Listing listing = listings[i];

                for(int y = 0; y < listing.images.Length; y++) {
                    Listing.ImageObject image = listing.images[y];

                    UnityWebRequest www = UnityWebRequestTexture.GetTexture(image.url);
                    yield return www.SendWebRequest();

                    if (www.result != UnityWebRequest.Result.Success) {
                        Debug.Log(www.result);
                        image.isLoaded = false;
                    }
                    else {
                        image.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                        image.isLoaded = true;
                    }
                    listing.images[y] = image;
                }
            }
        }
        
	}


	[Serializable]
	#nullable enable
	class MetacastAPIPage<T> 
	{
		public int count;
		public string? next;
		public List<T> results = new List<T>();
	}
}